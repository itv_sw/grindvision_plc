﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4022.18">
  <POU Name="FB_LogFifo" Id="{3b22ab44-54ef-4ac1-ad25-1b5bc2975657}" SpecialFunc="None">
    <Declaration><![CDATA[(* Log/debug/error message fifo *)
FUNCTION_BLOCK FB_LogFifo
VAR_INPUT
	bLog		: BOOL 		:= FALSE;(* TRUE => Enable log message output, FALSE => Disable *)
	sPrefix		: STRING 	:= 'Unknown::';(* Log message description string (allows the identification of log message source) *)
	
	sMessage	: STRING 	:= '';(* String message to add (write) to the fifo *)
	nErrID		: UDINT 	:= 0;(* Error code to add (write) to the fifo *)
	hSocket		: T_HSOCKET;(* Socket connection handle *)
END_VAR
VAR_OUTPUT
	bOk			: BOOL;(* TRUE = New entry added or removed succesfully, FALSE = Fifo overflow or fifo empty *)
	sGet		: T_MaxString := '';(* String entry removed (read) from fifo *)
	nCount		: UDINT := 0;(* Number of fifo entries *)
END_VAR
VAR
	fbBuffer 	: FB_StringRingBuffer;(* Basic (lower level) string buffer control function block *)
	buffer		: ARRAY[0..PLCPRJ_BUFFER_SIZE] OF BYTE;(* Internal buffer memory *)
	sSource		: T_MaxString;	
	sPut		: T_MaxString;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[;]]></ST>
    </Implementation>
    <Action Name="Clear" Id="{e7dd37d7-893c-48fc-ab6d-bfb0cbc1ef26}">
      <Implementation>
        <ST><![CDATA[(* Clears all fifo entries *)
fbBuffer.A_Reset( 	pBuffer := ADR( buffer ), cbBuffer := SIZEOF( buffer ), 
					bOk=>bOk, nCount=>nCount, getValue=>sGet ); 
]]></ST>
      </Implementation>
    </Action>
    <Action Name="Error" Id="{0ac8814c-0d69-4ec6-94f9-3e852a8b98a8}">
      <Implementation>
        <ST><![CDATA[(* Adds new fifo entry *)
IF nErrID = 0 THEN
	sSource := '::Error: 0x';
ELSIF ( nErrID AND 16#80000000) = 16#80000000 THEN
	IF nErrID = 16#80072746 THEN
		sSource := '::Win32 error (the connection is reset by remote side): 0x';
	ELSE
		sSource := '::Win32 error: 0x';
	END_IF
ELSIF (nErrID AND 16#00008100) = 16#00008100 THEN
	CASE nErrID OF
		PLCPRJ_ERROR_RECEIVE_TIMEOUT:
			sSource := '::PLC sample project receive timeout error: 0x';
		PLCPRJ_ERROR_SENDFIFO_OVERFLOW:
			sSource := '::PLC sample project send fifo overflow error: 0x';
		PLCPRJ_ERROR_RECFIFO_OVERFLOW:
			sSource := '::PLC sample project receive fifo overflow error: 0x';
		PLCPRJ_ERROR_INVALID_FRAME_FORMAT:
			sSource := '::PLC sample project invalid frame format error: 0x';
	ELSE
		sSource := '::Other PLC sample project error: 0x';
	END_CASE
ELSIF (nErrID AND 16#00008000) = 16#00008000 THEN
	sSource := '::Internal TCP/IP Connection Server error: 0x';
ELSE
	sSource := '::TwinCAT System error: 0x';
END_IF

sPut := CONCAT( CONCAT( CONCAT( sPrefix, sMessage ), sSource ), DWORD_TO_HEXSTR( nErrID AND 16#FFFF, 0, FALSE ) ); 

fbBuffer.A_AddTail( pBuffer:= ADR(buffer), cbBuffer:= SIZEOF(buffer),
					putValue:= sPut, bOk=>bOk, nCount=>nCount );
IF bOk THEN
	IF bLog THEN(* Log error message *)
		ADSLOGSTR( ADSLOG_MSGTYPE_ERROR OR ADSLOG_MSGTYPE_LOG, '%s', sPut );	
	END_IF
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="Message" Id="{e43c8d79-55cc-4889-94b9-518e64cc95a4}">
      <Implementation>
        <ST><![CDATA[(* Adds new fifo entry *)
sPut := CONCAT( CONCAT( sPrefix, sMessage ), CONCAT( '::', HSOCKET_TO_STRING(hSocket) ) );
fbBuffer.A_AddTail( pBuffer:= ADR(buffer), cbBuffer:= SIZEOF(buffer),
					putValue:= sPut, bOk=>bOk, nCount=>nCount );
IF bOk THEN
	IF bLog THEN(* Log information message *)
		ADSLOGSTR( ADSLOG_MSGTYPE_HINT OR ADSLOG_MSGTYPE_LOG, '%s', sPut );	
	END_IF
END_IF]]></ST>
      </Implementation>
    </Action>
    <Action Name="Remove" Id="{056a0109-71a8-4405-97f9-907b395dc64f}">
      <Implementation>
        <ST><![CDATA[(* Removes oldest fifo entry *)
IF nCount = 0 THEN
	sGet := '';
	bOk := FALSE;
	RETURN;
END_IF

fbBuffer.A_RemoveHead( pBuffer:= ADR(buffer), cbBuffer:= SIZEOF(buffer),
						getValue=>sGet, bOk=>bOk, nCount=>nCount );
]]></ST>
      </Implementation>
    </Action>
    <LineIds Name="FB_LogFifo">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_LogFifo.Clear">
      <LineId Id="2" Count="2" />
      <LineId Id="1" Count="0" />
    </LineIds>
    <LineIds Name="FB_LogFifo.Error">
      <LineId Id="2" Count="35" />
      <LineId Id="1" Count="0" />
    </LineIds>
    <LineIds Name="FB_LogFifo.Message">
      <LineId Id="2" Count="7" />
      <LineId Id="1" Count="0" />
    </LineIds>
    <LineIds Name="FB_LogFifo.Remove">
      <LineId Id="2" Count="8" />
      <LineId Id="1" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>