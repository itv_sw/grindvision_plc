﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4020.12">
  <POU Name="FB_LocalClient" Id="{6b804cdf-9766-4d78-85a2-3d321545a50e}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_LocalClient
VAR_INPUT
	sRemoteHost			: STRING(15);					(* IP adress of remote server *)
	nRemotePort			: UDINT := 9600;
	stFrameToServer		: ST_FINS_Frame;
	bEnable				: BOOL;	
	eCmd				: E_CmdFINS;
	nrWord				: UINT;
END_VAR
VAR_OUTPUT
	bConnected			: BOOL;
	hSocket				: T_HSOCKET;
	bBusy 				: BOOL;
	bReceiveOK			: BOOL;
	bError				: BOOL;
	nErrId				: UDINT;
	stFrameFromServer	: ST_FINS_Frame;
END_VAR
VAR_IN_OUT
	bSendCommand		: BOOL;
END_VAR
VAR
	fbConnect 			: FB_SocketConnect := ( sSrvNetId := ''  );
	fbClose				: FB_SocketClose := ( sSrvNetId := '', tTimeout := DEFAULT_ADS_TIMEOUT );
	fbClientDataExcha	: FB_ClientDataExcha;

	fbConnectTON		: TON := ( PT := PLCPRJ_RECONNECT_T );
	fbDataExchaTON		: TON := ( PT := PLCPRJ_SEND_CYCLE_T );
	eStep				: E_ClientSteps;
	nS					: UDINT;(* Send frame counter *)
	nR					: UDINT;(* Received frame counter *)
	nErr				: UDINT;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[CASE eStep OF

	CLIENT_STATE_IDLE:
		IF bEnable XOR bConnected THEN
			bBusy := TRUE;
			bError := FALSE;
			nErrid := 0;
			ClearFrame(stFrame:=stFrameToServer);
			IF bEnable THEN
				fbConnectTON( IN := FALSE );
				eStep := CLIENT_STATE_CONNECT_START;
			ELSE
				eStep := CLIENT_STATE_CLOSE_START;
			END_IF
		ELSIF bConnected THEN
			fbDataExchaTON( IN := FALSE );
			eStep := CLIENT_STATE_DATAEXCHA_START;
			nS:=nS+1;
		ELSE
			bBusy := FALSE;
		END_IF

	CLIENT_STATE_CONNECT_START:
		fbConnectTON( IN := TRUE, PT := PLCPRJ_RECONNECT_TIME );
		IF fbConnectTON.Q THEN
			fbConnectTON( IN := FALSE );
			fbConnect(  bExecute := FALSE  );
			fbConnect(	sRemoteHost		:= sRemoteHost,
						nRemotePort		:= nRemotePort,
						bExecute		:= TRUE );
			eStep := CLIENT_STATE_CONNECT_WAIT;
		END_IF

	CLIENT_STATE_CONNECT_WAIT:
		fbConnect( bExecute := FALSE );
		IF NOT fbConnect.bBusy THEN
			IF NOT fbConnect.bError THEN
				bConnected 	:= TRUE;
				hSocket 	:= fbConnect.hSocket;
				eStep 		:= CLIENT_STATE_IDLE;
				LogMessage( 'LOCAL client CONNECTED!', hSocket );
				nR 			:= 0;(* Reset receive frame counter *)
				nS 			:= 0;(* Reset send frame counter *)
				nErr		:= 0;
			ELSE
				LogError( 'FB_SocketConnect', fbConnect.nErrId );
				nErrId := fbConnect.nErrId;
				eStep := CLIENT_STATE_ERROR;
			END_IF
		END_IF

	CLIENT_STATE_DATAEXCHA_START:
		fbDataExchaTON( IN := TRUE, PT := PLCPRJ_SEND_CYCLE_T );
		bReceiveOK  := FALSE;
		//IF fbDataExchaTON.Q AND bCommand THEN
		IF bSendCommand THEN
			fbDataExchaTON( IN := FALSE );
			fbClientDataExcha( bExecute := FALSE );
			fbClientDataExcha( 	hSocket 	:= hSocket,
								stFrameToServer 	:= stFrameToServer,
								nrWord				:= nrWord,
								eCmd				:= eCmd,
								bExecute 	:= TRUE );
			bSendCommand:=FALSE;
			eStep := CLIENT_STATE_DATAEXCHA_WAIT;
		END_IF

	CLIENT_STATE_DATAEXCHA_WAIT:
		fbClientDataExcha( bExecute := FALSE );
		bReceiveOK  := FALSE;
		IF NOT fbClientDataExcha.bBusy THEN
			IF NOT fbClientDataExcha.bError THEN
				//ClearFrame(stFrame:=stFrameFromServer);
				stFrameFromServer := fbClientDataExcha.stFrameFromServer;
				bReceiveOK  := fbClientDataExcha.bFrameReceive;
				nR:=nR+1;
				eStep:= CLIENT_STATE_IDLE;
			ELSE
				(* possible errors are logged inside of fbClientDataExcha function block *)
				nErrId := fbClientDataExcha.nErrId;
				eStep :=CLIENT_STATE_ERROR;
			END_IF
		END_IF

	CLIENT_STATE_CLOSE_START:
		fbClose( bExecute := FALSE );
		fbClose(	hSocket:= hSocket,
				bExecute:= TRUE );
		eStep := CLIENT_STATE_CLOSE_WAIT;

	CLIENT_STATE_CLOSE_WAIT:
		fbClose( bExecute := FALSE );
		IF NOT fbClose.bBusy THEN
			LogMessage( 'LOCAL client CLOSED!', hSocket );
			bConnected := FALSE;
			MEMSET( ADR(hSocket), 0, SIZEOF(hSocket));
			IF fbClose.bError THEN
				LogError( 'FB_SocketClose (local client)', fbClose.nErrId );
				nErrId := fbClose.nErrId;
				eStep := CLIENT_STATE_ERROR;
			ELSE
				bBusy := FALSE;
				bError := FALSE;
				nErrId := 0;
				eStep := CLIENT_STATE_IDLE;
			END_IF
		END_IF

	CLIENT_STATE_ERROR: (* Error step *)
		bError := TRUE;
		nErr:=nErr+1;
		bReceiveOK  := FALSE;
		IF bConnected THEN
			eStep := CLIENT_STATE_CLOSE_START;
		ELSE
			bBusy := FALSE;
			eStep := CLIENT_STATE_IDLE;
		END_IF
END_CASE
]]></ST>
    </Implementation>
    <Method Name="ClearFrame" Id="{69a1b967-cbcd-4a72-863c-a5b206757277}">
      <Declaration><![CDATA[METHOD ClearFrame
VAR_IN_OUT
	stFrame		: ST_FINS_Frame;
END_VAR
VAR
	i:			INT;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[FOR i:=0 TO SIZEOF(stFrame.data)-1 DO
	stFrame.Data[i]:=0;
END_FOR]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="FB_LocalClient">
      <LineId Id="24" Count="16" />
      <LineId Id="249" Count="0" />
      <LineId Id="41" Count="22" />
      <LineId Id="243" Count="0" />
      <LineId Id="248" Count="0" />
      <LineId Id="253" Count="0" />
      <LineId Id="64" Count="8" />
      <LineId Id="293" Count="0" />
      <LineId Id="73" Count="0" />
      <LineId Id="209" Count="0" />
      <LineId Id="74" Count="3" />
      <LineId Id="181" Count="1" />
      <LineId Id="78" Count="0" />
      <LineId Id="213" Count="0" />
      <LineId Id="79" Count="4" />
      <LineId Id="294" Count="0" />
      <LineId Id="84" Count="1" />
      <LineId Id="178" Count="0" />
      <LineId Id="86" Count="0" />
      <LineId Id="259" Count="0" />
      <LineId Id="250" Count="0" />
      <LineId Id="87" Count="33" />
      <LineId Id="251" Count="0" />
      <LineId Id="258" Count="0" />
      <LineId Id="121" Count="7" />
    </LineIds>
    <LineIds Name="FB_LocalClient.ClearFrame">
      <LineId Id="5" Count="0" />
      <LineId Id="10" Count="1" />
    </LineIds>
  </POU>
</TcPlcObject>